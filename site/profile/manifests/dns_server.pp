class profile::dns_server {

  include dns::server

  # Configure puppetdb and its underlying database
  class { 'puppetdb': }
  # Configure the Puppet master to use puppetdb
  class { 'puppetdb::master::config': }

  dns::server::options { '/etc/bind/named.conf.options':
    #forwarders => [ '8.8.8.8', '8.8.4.4' ]
  }

  dns::zone { 'eivind.test':
    soa         => 'master.eivind.test',
    soa_email   => 'what.an.email',
    nameservers => [ 'master' ],
  }

  dns::zone { '159.168.192.IN-ADDR.ARPA':
    soa         => 'master.eivind.test',
    soa_email   => 'what.an.email',
    nameservers => ['master']
  }

  # Collect all the records from other nodes
  Dns::Record::A <<||>> 

}

