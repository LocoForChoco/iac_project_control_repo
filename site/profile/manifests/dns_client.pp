class profile::dns_client {

  @@dns::record::a { $::hostname:
    zone => 'eivind.test',
    data => $::ipaddress,
  }

# ugly hack
  exec { '/usr/sbin/netplan apply': }

}
